import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Root',
      components: {
        default: () => import ('@/components/Index'),
      },
      children: [
        {
          path: '',
          name: 'Home',
          components:{
            two: () => import ('@/components/Project')
          }
        }
      ]
    },
    {
      path: '/Admin',
      component: () => import ('@/components/Index'),
      children: [
            {
              path: '',
              name: 'Admin',
              component: () => import ('@/components/Admin')
            },
            {
              path: 'Product',
              component: () => import ('@/components/Admin'),
              children: 
              [
                {
                  path: '',
                  name: 'Product',
                  components: 
                  {
                    default: () => import ('@/components/Admin'),
                    admin :() => import ('@/components/Product'),
                  },
                },
                {
                  path: ':id',
                  name: 'ProductDetails',
                  components: 
                  { 
                    default: () => import ('@/components/Admin'),
                    admin :() => import ('@/components/ProductDetails')
                  }, 
                },
              ]     
            },
            {
              path: 'Category',
              component: () => import ('@/components/Admin'),
              children: [
                {
                  path: '',
                  name: 'Category',
                  components: 
                  {
                    default: () => import ('@/components/Admin'),
                    admin :() => import ('@/components/Category'),
                  },
                },
                {
                  path: ':id',
                  name: 'CategoryDetails',
                  components: 
                  { 
                    default: () => import ('@/components/Admin'),
                    admin :() => import ('@/components/CategoryDetails')
                  }, 
                },
              ]
            }
        ]
    },
    {
      path: '/Header',
      name: 'Header',
      component: () =>
        import ('@/components/Header')
    },
    {
      path: '/Footer',
      name: 'Footer',
      component: () =>
        import ('@/components/Footer')
    },
    {
      path: '/AboutProject',
      name: 'AboutProject',
      component: () => import ('@/components/Project')
    }
    
  ]
})
